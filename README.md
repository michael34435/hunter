demo用專案
---

## guide
本專案為純PHP打造一個簡單的router寫法，需使用`docker`以及`docker-compose`執行

## installation
```bash
docker-compose up -d --force-recreate --build
```

完成之後會監聽port80並且起在本機上面
