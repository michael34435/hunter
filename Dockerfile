FROM ubuntu:16.04

ENV LANG=C.UTF-8

RUN apt-get update && \
    apt-get install -y software-properties-common python-software-properties && \
    add-apt-repository -y ppa:ondrej/php && \
    add-apt-repository -y ppa:nginx/development && \
    apt-get update && \
    apt-get install -y php7.1 php7.1-fpm php7.1-mysql php7.1-gd nginx mysql-client


COPY database.sql database.sql

COPY nginx-default.conf /etc/nginx/sites-enabled/default

COPY www.conf /etc/php/7.1/fpm/pool.d/www.conf

COPY docker-entry.sh docker-entry.sh

RUN mkdir /var/run/php

CMD ["bash", "docker-entry.sh"]
