<?php

if (!defined('INDEX')) {
    exit(1);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email    = $_POST['email'] ?? '';
    $password = $_POST['password'] ?? '';

    if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
        if (strlen($password) > 0) {
            $prepare = $dbh->prepare('SELECT * FROM users WHERE email=:email');
            $prepare->bindParam(':email', $email, PDO::PARAM_STR);
            $prepare->execute();
            $user = $prepare->fetch();

            if ($user) {
                echo '此帳號無法使用';
            } else {
                $prepare = $dbh->prepare('INSERT INTO users(email, password) VALUES (:email, :password)');
                $prepare->bindParam(':email', $email, PDO::PARAM_STR);
                $prepare->bindParam(':password', password_hash($password, PASSWORD_BCRYPT), PDO::PARAM_STR);
                $prepare->execute();

                header('Location: /login');
            }
        } else {
            echo '請輸入密碼';
        }
    } else {
        echo '此帳號無法使用';
    }
}

echo '註冊畫面';
echo '<form method="POST">';
echo '<input name="email">';
echo '<input name="password" type="password">';
echo '<input type="submit">';
echo '</form>';
