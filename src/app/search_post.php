<?php

if (!defined('INDEX')) {
    exit(1);
}

$query = $_GET['q'] ?? '';
$page  = $_GET['page'] ?? 1;
$page   = (int) $page;
$page   = $page < 1 ? 1 : $page;
$offset = ($page - 1) * 10;


$prepare = $dbh->prepare('SELECT * FROM posts WHERE (title LIKE :title OR content LIKE :content) LIMIT 10 OFFSET :offset');
$prepare->bindParam(':title', '%'.$q.'%', PDO::PARAM_STR);
$prepare->bindParam(':content', '%'.$q.'%', PDO::PARAM_STR);
$prepare->bindValue(':offset', $offset, PDO::PARAM_INT);
$prepare->execute();
$posts = $prepare->fetchAll();


if ($posts) {
    echo '<form>';
    echo '<input name="q" placeholder="關鍵字">';
    echo '<input type="submit">';
    echo '</form>';
    echo '<ul>';
    foreach ($posts as $post) {
        echo '<li><a href="/post/'.$post['id'].'">'.$post['title'].'</a></li>';
    }
    echo '</ul>';
    echo '<a href="/search_post?page='.($page - 1).'&q='.$q.'">上一頁</a>';
    echo '<a href="/search_post?page='.($page + 1).'&q='.$q.'">下一頁</a>';
} else {
    echo '找不到任何文章';
}
