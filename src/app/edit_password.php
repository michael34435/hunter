<?php

if (!defined('INDEX') ) {
    exit(1);
}

$email = $_GET['email'] ?? $_POST['email'] ?? '';
$token = $_GET['token'] ?? $_POST['token'] ?? '';


$prepare = $dbh->prepare('SELECT * FROM users WHERE email=:email');
$prepare->bindParam(':email', $email, PDO::PARAM_STR);
$prepare->execute();
$user = $prepare->fetch();

if ($user) {
    if ($user['token'] != $token) {
        echo '更新失敗';
    } elseif (time() - strtotime($user['forgot_time']) < 600) {
        echo '已過期';
    } else {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $password     = $_POST['password'] ?? '';
            $new_password = $_POST['new_password'] ?? '';

            if (password_verify($user['password'], $password)) {
                $prepare = $dbh->prepare('UPDATE users SET password=:password WHERE email=:email');
                $prepare->bindParam(':password', password_hash($new_password, PASSWORD_BCRYPT), PDO::PARAM_STR);
                $prepare->execute();
            } else {
                echo '舊密碼跟新密碼不符';
            }
        }

        echo '<form method="POST">';
        echo '<input type="hidden" name="email" value="'.$email.'">';
        echo '<input type="hidden" name="token" value="'.$token.'">';
        echo '<input type="password" name="password" placeholder="舊密碼">';
        echo '<input type="password" name="new_password" placeholder="新密碼">';
        echo '<input type="submit">';
        echo '</form>';
    }
}
