<?php

if (!defined('INDEX')) {
    exit(1);
}

require '../tool/mail.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = $_POST['email'] ?? '';

    // check email is exist or not
    $prepare = $dbh->prepare('SELECT * FROM users WHERE email=:email');
    $prepare->bindParam(':email', $email, PDO::PARAM_STR);
    $prepare->execute();
    $user = $prepare->fetch();

    // send email
    if ($user) {
        $token = md5(uniqid());
        $prepare = $dbh->prepare('UPDATE users SET token=:token, forgot_time=:forgot_time WHERE email=:email');
        $prepare->bindParam(':email', $email, PDO::PARAM_STR);
        $prepare->bindParam(':token', $token, PDO::PARAM_STR);
        $prepare->bindParam(':forgot_time', date('Y-m-d H:i:s'), PDO::PARAM_STR);
        $prepare->execute();

        mail_send($email, '忘記密碼信', '點開: http://localhost/edit_password?token='.$token.'&email='.$email);

        echo '已經寄出忘記密碼信';
    }
}

echo '<form method="POST">';
echo '<input name="email" placeholder="請輸入帳號">';
echo '<input type="submit">';
echo '</form>';
