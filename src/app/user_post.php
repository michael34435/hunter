<?php

if (!defined('INDEX')) {
    exit(1);
}

$uid = array_shift($url);

if (!$uid) {
    exit(1);
}

$uid    = (int) $uid;
$page   = $_GET['page'] ?? 1;
$page   = (int) $page;
$page   = $page < 1 ? 1 : $page;
$offset = ($page - 1) * 10;

$prepare = $dbh->prepare('SELECT * FROM posts WHERE user_id=:user_id LIMIT 10 OFFSET :offset');
$prepare->bindParam(':user_id', $uid, PDO::PARAM_INT);
$prepare->bindValue(':offset', $offset, PDO::PARAM_INT);
$prepare->execute();
$posts = $prepare->fetchAll();

if ($posts) {
    echo '<ul>';
    foreach ($posts as $post) {
        echo '<li><a href="/post/'.$post['id'].'">'.$post['title'].'</a></li>';
    }
    echo '</ul>';
    echo '<a href="/user_post/'.$uid.'?page='.($page - 1).'">上一頁</a>';
    echo '<a href="/user_post/'.$uid.'?page='.($page + 1).'">下一頁</a>';
} else {
    echo '該使用者沒有文章';
}
