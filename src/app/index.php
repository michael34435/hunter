<?php

session_start();
define('INDEX', true);

$dbh = new PDO('mysql:host=hunter-mysql;dbname=test;port=3306', 'root', 'root', [
    PDO::ATTR_PERSISTENT => true
]);

$url = $_GET['url'] ?? '';
$url = explode('/', $url);

$controller = array_shift($url);
$controller = array_shift($url);

if ($controller) {
    $controller = strtolower($controller);
    $controller = __DIR__.'/'.$controller.'.php';

    if (file_exists($controller)) {
        require $controller;
    }
} else {
    header('Location: /login');
}
