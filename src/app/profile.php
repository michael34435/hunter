<?php

if (!defined('INDEX')) {
    exit(1);
}

if (!isset($_SESSION['uid'])) {
    exit(1);
}

$profile_pic = __DIR__.'/../image/'.$_SESSION['uid'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $file = $_FILES['file'] ?? '';

    if ($file) {
        $type = exif_imagetype($file['tmp_name']);

        switch ($type) {
            case IMAGETYPE_GIF:
            case IMAGETYPE_JPEG:
            case IMAGETYPE_PNG:
                move_uploaded_file($file['tmp_name'], $profile_pic);
                imagepng(imagecreatefromstring(file_get_contents($profile_pic)), $profile_pic);
                break;

            default:
                # code...
                break;
        }
    }
}

$prepare = $dbh->prepare('SELECT * FROM users WHERE id=:id');
$prepare->bindParam(':id', $_SESSION['uid'], PDO::PARAM_INT);
$prepare->execute();
$user = $prepare->fetch();

echo 'EMAIL: '.$user['email'];
echo '<img src="data:image/png;base64, '.base64_encode(file_get_contents($profile_pic)).'">';
echo '<form enctype="multipart/form-data" method="POST"><input type="file" name="file"><input type="submit"></form>';
echo '<a href="/post">回到列表</post>';
