<?php

if (!defined('INDEX')) {
    exit(1);
}

$post_id = array_shift($url);

if ($post_id) {
    $prepare = $dbh->prepare('SELECT * FROM posts WHERE id=:id');
    $prepare->bindParam(':id', $post_id, PDO::PARAM_INT);
    $prepare->execute();
    $post = $prepare->fetch();

    if ($post) {
        echo '<h2>標題:'.$post['title'].'</h2>';
        echo '<p>內文:'.$post['content'].'</p>';
        echo '<a href="/edit_post/'.$post['id'].'">修改</a>';
    }
} else {
    $prepare = $dbh->prepare('SELECT * FROM posts');
    $prepare->execute();
    $posts = $prepare->fetchAll();

    echo '<ul>';
    foreach ($posts as $post) {
        echo '<li><a href="/post/'.$post['id'].'">'.$post['title'].'</a></li>';
    }
    echo '</ul>';
    echo '<a href="/edit_post">新增貼文</a>';
    echo '<hr>';
    echo '<a href="/user_post/'.$_SESSION['uid'].'">本作者貼文</a>';
    echo '<hr>';
    echo '<a href="/profile">本作者</a>';
}
