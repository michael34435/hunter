<?php

if (!defined('INDEX') ) {
    exit(1);
}

if (!isset($_SESSION['uid'])) {
    exit(1);
}

$delete = $_GET['delete'] ?? false;

$title   = $_POST['title'] ?? '';
$content = $_POST['content'] ?? '';
$post_id = array_shift($url) ?: 0;

$prepare = $dbh->prepare('SELECT * FROM posts WHERE id=:id');
$prepare->bindParam(':id', $post_id, PDO::PARAM_INT);
$prepare->execute();
$post = $prepare->fetch();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($post) {
        if ($post['user_id'] != $_SESSION['uid']) {
            echo '非本人無法修改';
            exit();
        }

        $prepare = $dbh->prepare('UPDATE posts SET title=:title, content=:content WHERE id=:id');
        $prepare->bindParam(':title', htmlspecialchars($title), PDO::PARAM_STR);
        $prepare->bindParam(':content', htmlspecialchars($content), PDO::PARAM_STR);
        $prepare->bindParam(':id', $post_id, PDO::PARAM_INT);
        $prepare->execute();
    } else {
        $prepare = $dbh->prepare('INSERT INTO posts(title, content, user_id) VALUES (:title, :content, :user_id)');
        $prepare->bindParam(':title', htmlspecialchars($title), PDO::PARAM_STR);
        $prepare->bindParam(':content', htmlspecialchars($content), PDO::PARAM_STR);
        $prepare->bindParam(':user_id', $_SESSION['uid'], PDO::PARAM_INT);
        $prepare->execute();
    }

    header('Location: /post');
} elseif ($post) {
    if ($delete) {
        $prepare = $dbh->prepare('DELETE FROM posts WHERE user_id=:user_id AND id=:id');
        $prepare->bindParam(':user_id', $_SESSION['uid'], PDO::PARAM_INT);
        $prepare->bindParam(':id', $post_id, PDO::PARAM_INT);
        $prepare->execute();

        header('Location: /post');
    } else {
        $title = $post['title'];
        $content = $post['content'];
    }
}

echo '<form method="POST">';
echo '<input name="title" value="'.htmlspecialchars_decode($title).'" placeholder="請輸入標題">';
echo '<textarea name="content" placeholder="請輸入內文">'.htmlspecialchars_decode($content).'</textarea>';
echo '<input type="submit">';
echo '</form>';
if ($post && $post['user_id'] == $_SESSION['uid']) {
    echo '<a href="/post/'.$post_id.'?delete=1">刪除貼文</a>';
}
