<?php

if (!defined('INDEX')) {
    exit(1);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email    = $_POST['email'] ?? '';
    $password = $_POST['password'] ?? '';

    if ($email && $password) {
        $prepare = $dbh->prepare('SELECT * FROM users WHERE email=:email');
        $prepare->bindParam(':email', $email, PDO::PARAM_STR);
        $prepare->execute();
        $user = $prepare->fetch();

        if ($user && password_verify($password, $user['password'])) {
            $_SESSION['uid'] = $user['id'];
            header('Location: /post');
        } else {
            echo '帳號或是密碼錯誤';
        }
    } else {
        echo '請輸入帳號或是密碼';
    }
}


echo '登入畫面';
echo '<form method="POST">';
echo '<input name="email">';
echo '<input name="password" type="password">';
echo '<input type="submit">';
echo '</form>';
echo '<a href="/register">註冊</a>';
echo '<a href="/forgot_password">忘記密碼</a>';
