<?php

function mail_send($to, $subject, $message) {

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, 'api:key-bceb547114013a2a0942919e49da1314');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $plain = strip_tags(br2nl($message));

  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/sandboxf9985b2698a04c45958b22d3001634c2.mailgun.org/messages');
  curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => 'postmaster@sandboxf9985b2698a04c45958b22d3001634c2.mailgun.org',
        'to' => $to,
        'subject' => $subject,
        'html' => $message,
        'text' => $plain));

  $j = json_decode(curl_exec($ch));

  $info = curl_getinfo($ch);

  if($info['http_code'] != 200)
    exit(1);

  curl_close($ch);

  return $j;
}
